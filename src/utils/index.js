const POST_STUDENT_VALIDATION = {
  firstname: {
    exists: {
      errorMessage: "firstname is required",
      bail: true,
    },
    isString: {
      errorMessage: "firstname should be string",
      bail: true,
    },
    notEmpty: {
      errorMessage: "firstname should not be empty",
      bail: true,
    },
  },
  lastname: {
    exists: {
      errorMessage: "lastname is required",
      bail: true,
    },
    isString: {
      errorMessage: "lastname should be string",
      bail: true,
    },
    notEmpty: {
      errorMessage: "firstname should not be empty",
      bail: true,
    },
  },
  dateOfBirth: {
    exists: {
      errorMessage: "dateOfBirth is required",
      bail: true,
    },
    notEmpty: {
      errorMessage: "dateOfBirth should not be empty",
      bail: true,
    },
    isDate: {
      errorMessage: "dateOfBirth should be date",
      bail: true,
    },
    toDate: true,
  },
  course: {
    exists: {
      errorMessage: "course is required",
      bail: true,
    },
    isString: {
      errorMessage: "course should be string",
      bail: true,
    },
    notEmpty: {
      errorMessage: "course should not be empty",
      bail: true,
    },
  },
  hours: {
    exists: {
      errorMessage: "hours is required",
      bail: true,
    },
    notEmpty: {
      errorMessage: "hours should not be empty",
      bail: true,
    },
    isInt: {
      errorMessage: "hours should be number",
      bail: true,
    },
    toInt: true,
  },
  price: {
    exists: {
      errorMessage: "price is required",
      bail: true,
    },
    notEmpty: {
      errorMessage: "price should not be empty",
      bail: true,
    },
    isInt: {
      errorMessage: "hours should be number",
      bail: true,
    },
    toInt: true,
  },
};

const PATCH_STUDENT_VALIDATION = {
  firstname: {
    optional: true,
    notEmpty: {
      errorMessage: "firstname should not be empty",
      bail: true,
    },
    isString: {
      errorMessage: "firstname should be string",
      bail: true,
    },
  },
  lastname: {
    optional: true,
    notEmpty: {
      errorMessage: "firstname should not be empty",
      bail: true,
    },
    isString: {
      errorMessage: "lastname should be string",
      bail: true,
    },
  },
  dateOfBirth: {
    optional: true,

    exists: {
      errorMessage: "dateOfBirth is required",
      bail: true,
    },
    notEmpty: {
      errorMessage: "dateOfBirth should not be empty",
      bail: true,
    },
    isDate: {
      errorMessage: "dateOfBirth should be date",
      bail: true,
    },
    toDate: true,
  },
  course: {
    optional: true,
    exists: {
      errorMessage: "course is required",
      bail: true,
    },
    notEmpty: {
      errorMessage: "course should not be empty",
      bail: true,
    },
    isString: {
      errorMessage: "course should be string",
      bail: true,
    },
  },
  hours: {
    optional: true,
    isInt: {
      errorMessage: "hours should be number",
      bail: true,
    },
    toInt: true,
    exists: {
      errorMessage: "hours is required",
      bail: true,
    },
    notEmpty: {
      errorMessage: "hours should not be empty",
      bail: true,
    },
  },
  price: {
    optional: true,
    notEmpty: {
      errorMessage: "price should not be empty",
      bail: true,
    },
    isInt: {
      errorMessage: "hours should be number",
      bail: true,
    },
    toInt: true,
  },
};

module.exports = {
  PATCH_STUDENT_VALIDATION,
  POST_STUDENT_VALIDATION,
};
