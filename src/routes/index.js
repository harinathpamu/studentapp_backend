const express = require("express");
const router = express.Router();

const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const {
  validationResult,
  check,
  checkSchema,
  matchedData,
} = require("express-validator");

const {
  POST_STUDENT_VALIDATION,
  PATCH_STUDENT_VALIDATION,
} = require("../utils");

/* Ping. */
router.get("/ping", function (req, res, next) {
  res.sendStatus(200);
});

/* GET All Students */
router.get("/students", function (req, res, next) {
  prisma.student
    .findMany()
    .then((students) => {
      res.json(students);
    })
    .catch((error) => {
      res.status(500).end();
    });
});

/* GET Student By Id */
router.get(
  "/student/:id",
  async (req, res, next) => {
    await check("id")
      .toInt()
      .isNumeric()
      .withMessage("id must be number")
      .bail()
      .run(req);
    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }
    res.status(400).json({ errors: errors.array() });
  },
  function (req, res, next) {
    const id = req.params.id;
    prisma.student
      .findUnique({
        where: {
          id: +id,
        },
      })
      .then((student) => {
        if (student) {
          res.json(student);
        } else {
          res.status(404).end();
        }
      })
      .catch((error) => {
        res.status(404).end();
      });
  }
);

/* POST Add Student */
router.post(
  "/student",
  checkSchema(POST_STUDENT_VALIDATION),
  function (req, res, next) {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }
    res.status(400).json({ errors: errors.array() });
  },
  function (req, res, next) {
    const data = matchedData(req);
    prisma.student
      .create({
        data: {
          ...data,
        },
      })
      .then((student) => {
        res.json(student);
      })
      .catch((err) => {
        res.status(500).end();
      });
  }
);

/* PATCH Update Student By ID */
router.patch(
  "/student/:id",
  check("id").toInt().isNumeric().withMessage("id must be number").bail(),
  checkSchema(PATCH_STUDENT_VALIDATION),
  function (req, res, next) {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }
    res.status(400).json({ errors: errors.array() });
  },
  function (req, res, next) {
    const data = matchedData(req);
    const id = req.params.id;
    prisma.student
      .update({
        where: {
          id,
        },
        data: {
          ...data,
        },
      })
      .then((student) => {
        if (student) {
          res.json(student);
        } else {
          res.status(404).end();
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).end();
      });
  }
);

/* DELETE Student By ID */
router.delete(
  "/student/:id",
  async (req, res, next) => {
    await check("id")
      .toInt()
      .isNumeric()
      .withMessage("id must be number")
      .bail()
      .run(req);
    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }
    res.status(400).json({ errors: errors.array() });
  },
  function (req, res, next) {
    const id = req.params.id;
    prisma.student
      .delete({
        where: {
          id,
        },
      })
      .then((student) => {
        if (student) {
          res.json(student);
        } else {
          res.status(404).end();
        }
      })
      .catch((err) => {
        res.status(404).end();
      });
  }
);

module.exports = router;
