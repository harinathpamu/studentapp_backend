## API Documentation

[Documentation](https://documenter.getpostman.com/view/5153977/UVBzn9SG)

## Available Scripts

In the project directory, to run:

### `npm install`

Installs node_modules ( project dependencies )

### `npm start`

Runs the app in the production mode.\
Open [http://localhost:3000/api](http://localhost:3000/api) to view it in the browser.

### `npm dev`

Runs the app in the development mode.\
Open [http://localhost:3000/api](http://localhost:3000/api) to view it in the browser.

### `npm initialize-db-schema`

Intialises prisma schema.

### `npm prisma-studio`

Opens prisma studio to view data of database

## Docker

Install [docker](https://www.docker.com/)

After installing to build the docker image ,in the project directory

To build the docker image

### `docker build --pull --rm -f "Dockerfile" -t student_app_backend:latest "."`

To run the docker image

### `docker run -d -p 3000:3000 student_app_backend`

Visit [studentapp](http:localhost:3000/api/students)

## Libraries used

## ExpressJS

Fast, unopinionated, minimalist web framework for Node.js

See the section about [expressjs](https://expressjs.com/) for more information.

## Prisma

Next-generation Node.js and TypeScript ORM

- Prisma helps app developers build faster and make fewer errors with an open source database toolkit for PostgreSQL, MySQL, SQL Server, and SQLite

See the section about [prisma](https://www.prisma.io/) for more information.
